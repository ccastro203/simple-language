
Programa->Lista_Declaraciones Lista_Sentencias

Lista_Declaraciones-> Declaracion Lista_Declaraciones
                     |epsilon

Declaracion -> Tipo Array_Extension Id;

Tipo-> number
      |string
      
Lista_Sentencias->Sentencia Lista_Sentencias
                  |epsilon

Sentencia-> read id;
           |print ExpresionBooleana;
           |id = ExpresionBooleana;
           |for ID = ExpresionBooleana to ExpresionBooleana  Lista_Sentencias end

ExpresionBooleana-> Expresion ExpresionBooleanaP

ExpresionBooleanaP->== Expresion ExpresionBooleanaP
                    |> Expresion ExpresionBooleanaP
					|>= Expresion ExpresionBooleanaP
					|< Expresion ExpresionBooleanaP
					|<= Expresion ExpresionBooleanaP
					|!= Expresion ExpresionBooleanaP
					|epsilon

Expresion -> Factor ExpresionP

ExpresionP->     + Factor ExpresionP
                |- Factor ExpresionP
				|epsilon

Factor-> Termino FactorP

FactorP->     * Termino FactorP
              |/ Termino FactorP
		      |epsilon

Termino -> ID
          |numero
		  |(ExpresionBooleana)
		  |! Termino

ID-> id Lista_Accesor

Lista_Accesor-> SingleAccesor Lista_Accesor
               |epsilon

SingleAccesor -> (Lista_Expresiones)
          |.id

Lista_Expresiones -> Expresion Lista_Expresiones'

Lista_Expresiones'-> , Expresion Lista_Expresiones'
                    |e


   




