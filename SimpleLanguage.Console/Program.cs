﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleLanguage.Specs;

namespace SimpleLanguage.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            string code = @"
                               number base;
                               number exponente;
                               number potencia;
                               number i;
                               string nombre;
                               string apellido;
                               potencia=1;
                               read base;
                               read exponente;
                             
                               for i=1 to exponente 
                                  print i;
                                  potencia = potencia *base;
                                  exponente=exponente-1;
                               end
                               print potencia;

                              read nombre;
                              read apellido;
                              print nombre+apellido;

                           ";

            var parser = new Parser(new Lexer(new ContentReader {Content = code}));
            var sourceCode = parser.Parse();
            foreach (var statement in sourceCode)
                statement.ValidateSemantics();
            System.Console.WriteLine("Inicio");
            foreach (var statement in sourceCode)
                statement.Interpret();
            System.Console.WriteLine("Fin");
            System.Console.ReadKey();
        }
    }
}
