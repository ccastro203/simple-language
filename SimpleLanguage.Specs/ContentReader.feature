﻿Feature: ContentReader
	In order to obtain characters from a FLOW...
	As a programmer
	I want to make sure this thing is working 


Scenario: Obtain next symbol
	Given The code "A=B+C;" 
	And  1 symbols have been consumed 
	When I ask for the next symbol
	Then the current symbol should be "="

	Scenario: Obtain second symbol
	Given The code "A=B+C;" 
	When I ask for the next symbol
	Then the current symbol should be "A"

Scenario: Obtain end of file with empty code
	Given The code "" 
	When I ask for the next symbol
	Then the current symbol should be an end of file symbol

Scenario: Obtain end of file with non empty code
	Given The code "C=A+B;"
	And  6 symbols have been consumed 
	When I ask for the next symbol
	Then the current symbol should be an end of file symbol

Scenario: Obtain end of file when the current symbol is end of file
	Given The code "C=A+B;"
	And  7 symbols have been consumed 
	When I ask for the next symbol
	Then the current symbol should be an end of file symbol

	Scenario: Obtain line and column count from a single line of code
	Given The code "C=A+B;"
	And  4 symbols have been consumed 
	When I ask for the next symbol
	Then the line count is 1 and column count is 6  
