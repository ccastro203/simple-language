﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace SimpleLanguage.Specs
{
    [Binding]
    public class ContentReaderSteps
    {
        private ContentReader contentReader;
        private char currentSymbol;


        [Given(@"The code ""(.*)""")]
        public void GivenTheCode(string p0)
        {
           contentReader = new ContentReader{Content=p0};
        }

        [When(@"I ask for the next symbol")]
        public void WhenIAskForTheNextSymbol()
        {
             currentSymbol =  contentReader.NextSymbol();
        }

        [Then(@"the current symbol should be ""(.*)""")]
        public void ThenTheCurrentSymbolShouldBe(string p0)
        {
            Assert.AreEqual(p0[0],currentSymbol);
        }
        [Then(@"the current symbol should be an end of file symbol")]
        public void ThenTheCurrentSymbolShouldBeAnEndOfFileSymbol()
        {
            Assert.AreEqual('\0', currentSymbol);
        }

        [Given(@"(.*) symbols have been consumed")]
        public void GivenSymbolsHaveBeenConsumed(int p0)
        {
            for (int i = 0; i < p0; i++)
                currentSymbol = contentReader.NextSymbol();
        }

        [Then(@"the line count is (.*) and column count is (.*)")]
        public void ThenTheLineCountIsAndColumnCountIs(int p0, int p1)
        {
            Assert.AreEqual(p0, contentReader.CurrentLine);
            Assert.AreEqual(p1, contentReader.CurrentColumn);
        }


    }
}
