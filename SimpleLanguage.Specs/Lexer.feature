﻿Feature: Lexer
	In order to obtain tokens from a symbol stream...
	As a programmer
	I want to make sure this thing is working 



Scenario: Obtain an id token
	Given the character stream "goku" 
	When  I ask for the next token
	Then the current token type should be "Id"
	And  the current token lexeme should be "goku"

	
Scenario: Obtain an id token 2
	Given the character stream "goku2" 
	When  I ask for the next token
	Then the current token type should be "Id"
	And  the current token lexeme should be "goku2"

Scenario: Obtain an eof token
	Given the character stream "goku" 
	And  1 tokens have been consumed
	When  I ask for the next token
	Then the current token type should be "EndOfFile"

Scenario: Obtain an eof token as a third token
	Given the character stream "goku vegeta" 
	And  2 tokens have been consumed
	When  I ask for the next token
	Then the current token type should be "EndOfFile"

Scenario: Obtain an integer number token
	Given the character stream "857" 
	When  I ask for the next token
	Then the current token type should be "Number"
	And  the current token lexeme should be "857"
	     
	Scenario: Obtain a float number token
	Given the character stream "42.24" 
	When  I ask for the next token
	Then the current token type should be "Number"
	And  the current token lexeme should be "42.24"

	Scenario: Lexical error with a float number token
	Given the character stream "42." 
	When  I ask for the next token
	Then a lexical exception should occur 
    Scenario: Obtain single character tokens
	Given the character stream "; ( ) { }  - * + /" 
	When  I ask for all tokens
	Then the token list should be 
	     | TokenType        | Lexeme |
	     | SemiColon        | ;      |
	     | LeftParenthesis  | (      |
	     | RightParenthesis | )      |
	     | LeftCurlyBrace   | {      |
	     | RightCurlyBrace  | }      |
	     | Op_Substract     | -      |
	     | Op_Multiply      | *      |
		 | Op_Sum           | +      |
		 | Op_Divide           | /      |

    Scenario: HAHAHAHAH LOLOLOL XDXDXD 
	Given the character stream "12.1+hola" 
	When  I ask for all tokens
	Then the token list should be 
	     | TokenType        | Lexeme |
	     | Number        | 12.1    |
	     | Op_Sum  | +      |
	     | Id | hola     |

    Scenario: Obtain single and double character tokens
	Given the character stream "= < > ! == <= >= !=" 
	When  I ask for all tokens
	Then the token list should be 
	     | TokenType        | Lexeme |
	     | Op_Assign        | =      |
	     | Op_LessThan  | <      |
	     | Op_GreaterThan|>       |
	     | Op_Not |!       |
		 |Op_Equals  | ==      |
		 |Op_LessEqualThan  | <=       |
		 |Op_GreaterEqualThan  | >=       |
		 |Op_NotEquals  | !=       |

	Scenario: Obtain reserved words tokens
	Given the character stream "if read print else while" 
	When  I ask for all tokens
	Then the token list should be 
	     | TokenType        | Lexeme |
	     | Rw_If        | if      |
	     | Rw_Read      | read     |
	     | Rw_Print     | print       |
		 | Rw_Else     | else       |
	     | Rw_While     | while       |
	
	Scenario: Zedura no Densetsu
	Given the character stream "print A=(B+12.3)/(78);" 
	When  I ask for all tokens
	Then the token list should be 
	     | TokenType        | Lexeme |
		 | Rw_Print        | print      |
	     | Id        | A      |
	     | Op_Assign      |  =     |
	     | LeftParenthesis    | (       |
		 | Id     | B      |
	     | Op_Sum     | +       |
		 | Number        | 12.3      |
		 | RightParenthesis        | )      |
		 | Op_Divide        | /      |
		 | LeftParenthesis    | (       |
		 | Number        | 78      |
		 | RightParenthesis        | )      |
		 | SemiColon        | ;     |

	     
