﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace SimpleLanguage.Specs
{
    [Binding]
    public class LexerSteps
    {

        private Lexer lexer;
        private Token currentToken;
        private List<Token> tokenList=new List<Token>();
        private Exception currentException;

        [Given(@"the character stream ""(.*)""")]
        public void GivenTheCharacterStream(string p0)
        {
            lexer = new Lexer(new ContentReader{Content = p0});
        }

        [When(@"I ask for the next token")]
        public void WhenIAskForTheNextToken()
        {
            try
            {
                currentToken = lexer.NextToken();
            }
            catch (Exception e)
            {
                currentException = e;
            }
           
        }

        [Then(@"the current token type should be ""(.*)""")]
        public void ThenTheCurrentTokenTypeShouldBe(string p0)
        {
            if (currentException != null)
                throw currentException;
            Assert.AreEqual(p0,currentToken.Type.ToString());
        }

        [Then(@"the current token lexeme should be ""(.*)""")]
        public void ThenTheCurrentTokenLexemeShouldBe(string p0)
        {
            if (currentException != null)
                throw currentException;
            Assert.AreEqual(p0,currentToken.Lexeme);
        }

        [Given(@"(.*) tokens have been consumed")]
        public void GivenTokensHaveBeenConsumed(int p0)
        {
            for(int i=0;i<=p0;i++)
                currentToken = lexer.NextToken();
        }
        [When(@"I ask for all tokens")]
        public void WhenIAskForAllTokens()
        {
            currentToken = lexer.NextToken();
            while (currentToken.Type != TokenType.EndOfFile)
            {
                tokenList.Add(currentToken);
                currentToken = lexer.NextToken();
            }
            
        }

        [Then(@"the token list should be")]
        public void ThenTheTokenListShouldBe(Table table)
        {
            if (currentException != null)
                throw currentException;
            Assert.AreEqual(table.RowCount,tokenList.Count);
            for(int i=0;i<table.RowCount;i++)
            {
                 Assert.AreEqual(table.Rows[i]["TokenType"],tokenList[i].Type.ToString());
                 Assert.AreEqual(table.Rows[i]["Lexeme"], tokenList[i].Lexeme);
            }
        }
        [Then(@"a lexical exception should occur")]
        public void ThenALexicalExceptionShouldOccur()
        {
            Assert.IsInstanceOfType(currentException,typeof(LexicalException));
        }


    }
}
