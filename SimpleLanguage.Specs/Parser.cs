﻿using System;

namespace SimpleLanguage.Specs
{
    public class Parser
    {
        private Lexer _lexer;
        private Token currentToken;
        public Parser(Lexer lexer)
        {
            _lexer = lexer;
            currentToken = _lexer.NextToken();
        }

        public void Parse()
        {
            Programa();
            if (currentToken.Type != TokenType.EndOfFile)
                throw new ParserException("Se esperaba fin de codigo");
        }

        private void Programa()
        {
            Lista_Sentencias();
        }

        private void Lista_Sentencias()
        {
            //Sentencia Lista_Sentencias
            if (currentToken.Type == TokenType.Rw_Read || currentToken.Type == TokenType.Rw_Print|| currentToken.Type == TokenType.Id)
            {
                Sentencia();
                Lista_Sentencias();
            }
            //Epsilon
            else
            {

            }
        }

        private void Sentencia()
        {
            //read id;
            if (currentToken.Type == TokenType.Rw_Read)
            {
                currentToken = _lexer.NextToken();
                if (currentToken.Type != TokenType.Id)
                    throw new ParserException("Se esperaba Id");
                currentToken = _lexer.NextToken();
                if (currentToken.Type != TokenType.SemiColon)
                    throw new ParserException("Se esperaba ;");
                currentToken = _lexer.NextToken();
            }
            //print ExpresionBooleana;
            else if (currentToken.Type == TokenType.Rw_Print)
            {
                currentToken = _lexer.NextToken();
                ExpresionBooleana();
                if (currentToken.Type != TokenType.SemiColon)
                    throw new ParserException("Se esperaba ;");
                currentToken = _lexer.NextToken();
            }
            //id = ExpresionBooleana;
            else if (currentToken.Type == TokenType.Id)
            {
                currentToken = _lexer.NextToken();
                if (currentToken.Type != TokenType.Op_Assign)
                    throw new ParserException("Se esperaba =");
                currentToken = _lexer.NextToken();
                ExpresionBooleana();
                if (currentToken.Type != TokenType.SemiColon)
                    throw new ParserException("Se esperaba ;");
                currentToken = _lexer.NextToken();
            }
            //NO HAY EPSILON
            else
            {
                throw new ParserException("Se esperaba inicio de sentencia");
            }
        }

        private void ExpresionBooleana()
        {
           Expresion();
           ExpresionBooleanaP();
        }

        private void ExpresionBooleanaP()
        {
            //== Expresion ExpresionBooleanaP
            if (currentToken.Type == TokenType.Op_Equals)
            {
                currentToken = _lexer.NextToken();
                Expresion();
                ExpresionBooleanaP();
            }
            //< Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_LessThan)
            {
                currentToken = _lexer.NextToken();
                Expresion();
                ExpresionBooleanaP();
            }
            //<= Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_LessEqualThan)
            {
                currentToken = _lexer.NextToken();
                Expresion();
                ExpresionBooleanaP();
            }
            //> Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_GreaterThan)
            {
                currentToken = _lexer.NextToken();
                Expresion();
                ExpresionBooleanaP();
            }
            //>= Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_GreaterEqualThan)
            {
                currentToken = _lexer.NextToken();
                Expresion();
                ExpresionBooleanaP();
            }
            //!= Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_NotEquals)
            {
                currentToken = _lexer.NextToken();
                Expresion();
                ExpresionBooleanaP();
            }
            //epsilon
            else
            {
                
            }
        }

        private void Expresion()
        {
            Factor();
            ExpresionP();
        }

        private void ExpresionP()
        {
            //+ Factor ExpresionP
            if (currentToken.Type == TokenType.Op_Sum)
            {
                currentToken = _lexer.NextToken();
                Factor();
                ExpresionP();
            }
            //- Factor ExpresionP
            else if (currentToken.Type == TokenType.Op_Substract)
            {
                currentToken = _lexer.NextToken();
                Factor();
                ExpresionP();
            }
            //epsilon
            else
            {

            }
        }

        private void Factor()
        {
            Termino();
            FactorP();
        }

        private void FactorP()
        {
            //* Termino FactorP
            if (currentToken.Type == TokenType.Op_Multiply)
            {
                currentToken = _lexer.NextToken();
                Termino();
                FactorP();
            }
            // / Termino FactorP
            else if (currentToken.Type == TokenType.Op_Divide)
            {
                currentToken = _lexer.NextToken();
                Termino();
                FactorP();
            }
            //epsilon
            else
            {

            }
        }

        private void Termino()
        {
            if (currentToken.Type == TokenType.Id)
            {
                currentToken = _lexer.NextToken();
            }
            else if (currentToken.Type == TokenType.Number)
            {
                currentToken = _lexer.NextToken();
            }
            else if (currentToken.Type == TokenType.LeftParenthesis)
            {
                currentToken = _lexer.NextToken();
                ExpresionBooleana();
                if (currentToken.Type != TokenType.RightParenthesis)
                    throw new ParserException("Se esperaba un raito parentesis");
                currentToken = _lexer.NextToken();
            }
            else if (currentToken.Type == TokenType.Op_Not)
            {
                currentToken = _lexer.NextToken();
                Termino();
            }
            else
            {
                throw new ParserException("Se esperaba un termino");
            }
        }
    }
}