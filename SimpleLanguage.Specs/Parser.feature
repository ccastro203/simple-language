﻿Feature: Parser
	In order to validate syntax ...
	As a programmer
	I want to make sure this thing is working 


Scenario: Parse a read statement
	Given the source code "read x;" 
	When  the code is parsed
	Then the code is error free

Scenario: Parse mutiple read statements
	Given the source code "read x1;read x2;read x3;" 
	When  the code is parsed
	Then the code is error free

Scenario: Parse incorrect read statement
	Given the source code "read ;x" 
	When  the code is parsed
	Then  a parser error is found

Scenario: Parse a print statement
	Given the source code "print (x+2)/(a==b)/!4;" 
	When  the code is parsed
	Then the code is error free

Scenario: Parse a print and read statement
	Given the source code "read x; print x+2;" 
	When  the code is parsed
	Then the code is error free

Scenario: Parse an assign statement
	Given the source code "a=b*(c/2);" 
	When  the code is parsed
	Then the code is error free

Scenario: Parse a simple sum program
	Given the source code "read A;read B;C=A+B;print C;" 
	When  the code is parsed
	Then the code is error free

Scenario: Parse a simple program with declarations
	Given the source code "number A;number B;number C;read A;read B;C=A+B;print C;" 
	When  the code is parsed
	Then the code is error free

Scenario: Mucho Flow
	Given the source code "read A;" 
	When  the code is parsed
	Then the code is error free