﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace SimpleLanguage.Specs
{
    [Binding]
    public class ParserSteps
    {
        private Parser parser;
        private Exception currentException;

        [Given(@"the source code ""(.*)""")]
        public void GivenTheSourceCode(string p0)
        {
           parser = new Parser( new Lexer(new ContentReader{Content = p0}));
        }

        [When(@"the code is parsed")]
        public void WhenTheCodeIsParsed()
        {
            try
            {
                parser.Parse();
            }
            catch (Exception e)
            {
                currentException = e;
            }
           
        }

        [Then(@"the code is error free")]
        public void ThenTheCodeIsErrorFree()
        {
            Assert.IsNull(currentException);
        }

        [Then(@"a parser error is found")]
        public void ThenAParserErrorIsFound()
        {
            Assert.IsNotNull(currentException);
            Assert.IsInstanceOfType(currentException,typeof(ParserException));
        }

    }
}
