 namespace SimpleLanguage.Specs
{
    public enum TokenType
    {
        Id,
        EndOfFile,
        Number,
        SemiColon,
        LeftParenthesis,
        RightParenthesis,
        LeftCurlyBrace,
        RightCurlyBrace,
        Op_Sum,
        Op_Substract,
        Op_Multiply,
        Op_Equals,
        Op_LessThan,
        Op_GreaterThan,
        Op_Not,
        Op_Assign,
        Op_LessEqualThan,
        Op_GreaterEqualThan,
        Op_NotEquals,
        Rw_If,
        Rw_While,
        Rw_Read,
        Rw_Print,
        Rw_Else,
        Op_Divide
    }

    public class Token
    {
        public TokenType Type { get; set; }
        public string Lexeme { get; set; }
    }
}