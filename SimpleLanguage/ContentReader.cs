using System;

namespace SimpleLanguage
{
    public class ContentReader
    {
        public string Content { get; set; }
        public int CurrentLine { get; set; }
        public int CurrentColumn { get; set; }

        private int _currentPosition=0;

        public ContentReader()
        {
            CurrentLine = 1;
            CurrentColumn = 1;
        }

        public char NextSymbol()
        {
            if (_currentPosition < Content.Length)
            {
                var currentSymbol = Content[_currentPosition++];
                if (currentSymbol == '\n')
                {
                    CurrentLine++;
                    CurrentColumn = 1;
                }
                else
                    CurrentColumn++;
                
                return currentSymbol;
            }
            else
            {
                return '\0';
            }
        }
    }
}