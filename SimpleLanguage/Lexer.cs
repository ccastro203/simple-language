using System.Collections.Generic;

namespace SimpleLanguage.Specs
{
    public class Lexer
    {
        private ContentReader _contentReader;
        private char _currentSymbol;
        private SortedDictionary<string, TokenType> singleSymbolDictionary;
        private SortedDictionary<string, TokenType> doubleSymbolDictionary;
        private SortedDictionary<string, TokenType> reservedWordsDictionary;

        public Lexer(ContentReader contentReader)
        {
            _contentReader = contentReader;
            _currentSymbol = contentReader.NextSymbol();

            singleSymbolDictionary = new SortedDictionary<string, TokenType>
            {
                {";", TokenType.SemiColon},
                 {"(", TokenType.LeftParenthesis},
                  {")", TokenType.RightParenthesis},
                   {"{", TokenType.LeftCurlyBrace},
                    {"}", TokenType.RightCurlyBrace},
                     {"+", TokenType.Op_Sum},
                      {"-", TokenType.Op_Substract},
                       {"*", TokenType.Op_Multiply},
                       {"/", TokenType.Op_Divide},
                       {",", TokenType.Comma},
                       {".", TokenType.Dot}
            };

            doubleSymbolDictionary = new SortedDictionary<string, TokenType>
            {
                {"=", TokenType.Op_Assign},
                 {"<", TokenType.Op_LessThan},
                  {">", TokenType.Op_GreaterThan},
                   {"!", TokenType.Op_Not},
                    {"==", TokenType.Op_Equals},
                     {"<=", TokenType.Op_LessEqualThan},
                      {">=", TokenType.Op_GreaterEqualThan},
                       {"!=", TokenType.Op_NotEquals}
            };
            reservedWordsDictionary = new SortedDictionary<string, TokenType>
            {
                {"if", TokenType.Rw_If},
                 {"while", TokenType.Rw_While},
                  {"read", TokenType.Rw_Read},
                   {"print", TokenType.Rw_Print},
                    {"else", TokenType.Rw_Else},
                    {"number", TokenType.Rw_Number},
                    {"string", TokenType.Rw_String},
                     {"for", TokenType.Rw_For},
                      {"to", TokenType.Rw_To},
                       {"end", TokenType.Rw_End}
                    
            };
        }

        public Token NextToken()
        {
            string lexeme = "";
            int currentState = 0;
            while (true)
            {
                switch (currentState)
                {
                    case 0:
                        if (char.IsLetter(_currentSymbol))
                        {
                            lexeme += _currentSymbol;
                            currentState = 1;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else if (_currentSymbol == '\0')
                        {
                            currentState = 16;
                        }
                        else if (char.IsWhiteSpace(_currentSymbol))
                        {
                            currentState = 0;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else if (char.IsDigit(_currentSymbol))
                        {
                            lexeme += _currentSymbol;
                            currentState = 3;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else if (singleSymbolDictionary.ContainsKey(_currentSymbol.ToString()))
                        {
                            lexeme += _currentSymbol;
                            currentState = 7;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else if (doubleSymbolDictionary.ContainsKey(_currentSymbol.ToString()))
                        {
                            lexeme += _currentSymbol;
                            currentState = 8;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else
                            throw new LexicalException("Symbol " + _currentSymbol + " is not recognized");
                        break;
                    case 1:
                        if (char.IsLetterOrDigit(_currentSymbol))
                        {
                            lexeme += _currentSymbol;
                            currentState = 1;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else
                            currentState = 2;
                        break;
                    case 2:
                        if(reservedWordsDictionary.ContainsKey(lexeme))
                            return new Token { Lexeme = lexeme, Type=reservedWordsDictionary[lexeme] };
                        else
                            return new Token { Lexeme = lexeme, Type = TokenType.Id };
                        break;
                    case 3:
                        if (char.IsDigit(_currentSymbol))
                        {
                            lexeme += _currentSymbol;
                            currentState = 3;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else if (_currentSymbol == '.')
                        {
                            lexeme += _currentSymbol;
                            currentState = 4;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else
                            currentState = 6;
                        break;
                    case 4:
                        if (char.IsDigit(_currentSymbol))
                        {
                            lexeme += _currentSymbol;
                            currentState = 5;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else
                            throw new LexicalException("A digit was expected");
                        break;
                    case 5:
                        if (char.IsDigit(_currentSymbol))
                        {
                            lexeme += _currentSymbol;
                            currentState = 5;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else
                            currentState = 6;
                        break;
                    case 6:
                        return new Token { Lexeme = lexeme, Type = TokenType.Number };
                        break;
                    case 7:
                        return new Token { Lexeme = lexeme, Type = singleSymbolDictionary[lexeme] };
                        break;
                    case 8:
                        if (doubleSymbolDictionary.ContainsKey(lexeme+_currentSymbol))
                        {
                            lexeme += _currentSymbol;
                            currentState = 9;
                            _currentSymbol = _contentReader.NextSymbol();
                        }
                        else
                            currentState = 9;
                        break;
                    case 9:
                        return new Token { Lexeme = lexeme, Type = doubleSymbolDictionary[lexeme] };
                        break;
                   
                    case 16:
                        return new Token { Type = TokenType.EndOfFile };
                        break;
                }
            }

        }
    }
}