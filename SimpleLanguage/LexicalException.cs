    using System;

namespace SimpleLanguage.Specs
{
    public class LexicalException : Exception
    {
        public LexicalException(string s):base(s)
        {
            
        }
    }
}