﻿using System;
using System.Collections.Generic;
using SimpleLanguage.SyntaxTree.ExpresionTree;
using SimpleLanguage.SyntaxTree.StatementTree;
using SimpleLanguage.Types;

namespace SimpleLanguage.Specs
{
    public class Parser
    {
        private Lexer _lexer;
        private Token currentToken;
        public Parser(Lexer lexer)
        {
            _lexer = lexer;
            currentToken = _lexer.NextToken();
        }

        public List<StatementNode> Parse()
        {
            var statementList = Programa();
            if (currentToken.Type != TokenType.EndOfFile)
                throw new ParserException("Se esperaba fin de codigo");
            return statementList;
        }

        public float Evaluate()
        {
            var statementList = ExpresionBooleana();
            if (currentToken.Type != TokenType.EndOfFile)
                throw new ParserException("Se esperaba fin de codigo");
            return statementList.Evaluate();
        }

        private List<StatementNode> Programa()
        {
            Lista_Declaraciones();
            return Lista_Sentencias();
        }

        private void Lista_Declaraciones()
        {
            if (currentToken.Type == TokenType.Rw_Number||
                currentToken.Type == TokenType.Rw_String)
            {
                Declaracion();
                Lista_Declaraciones();
            }
            //epsilon
            else
            {
                
            }
        }

        private void Declaracion()
        {
            var type = Tipo();
            var arrayType =Array_Extension();
            var id = currentToken.Lexeme;
            if(currentToken.Type!=TokenType.Id)
                throw new ParserException("Se esperaba id");
            currentToken = _lexer.NextToken();
            if (currentToken.Type != TokenType.SemiColon)
                throw new ParserException("Se esperaba ;");
            currentToken = _lexer.NextToken();
            if (arrayType == null)
                SymbolsTable.Instance.DeclareVariable(id, type);
            else
            {
                arrayType.OfType = type;
                SymbolsTable.Instance.DeclareVariable(id,arrayType);
            }
        }

        private ArrayType Array_Extension()
        {
            if (currentToken.Type == TokenType.LeftParenthesis)
            {
                currentToken = _lexer.NextToken();
                var listaEnteros= Lista_Enteros();
                if (currentToken.Type != TokenType.RightParenthesis)
                    throw new ParserException("Se esperaba )");
                currentToken = _lexer.NextToken();
                return new ArrayType {Dimensions = listaEnteros};
            }
            //epsilon
            else
            {
                return null;
            }
        }

        private List<int> Lista_Enteros()
        {
            if (currentToken.Type != TokenType.Number)
                throw new ParserException("Se esperaba numero");
            var numberLexeme = currentToken.Lexeme;
            currentToken = _lexer.NextToken();
            var listaEntero= Lista_EnterosP();
            listaEntero.Insert(0, int.Parse(numberLexeme));
            return listaEntero;
        }

        private List<int> Lista_EnterosP()
        {
            if (currentToken.Type == TokenType.Comma)
            {
                currentToken = _lexer.NextToken();
                if (currentToken.Type != TokenType.Number)
                    throw new ParserException("Se esperaba numero");
                var numberLexeme = currentToken.Lexeme;
                currentToken = _lexer.NextToken();
                var listaEntero = Lista_EnterosP();
                listaEntero.Insert(0,int.Parse(numberLexeme));
                return listaEntero;
            }
            //epsilon
            else
            {
                return new List<int>();
            }
        }

        private BaseType Tipo()
        {
            if (currentToken.Type == TokenType.Rw_Number)
            {
                currentToken = _lexer.NextToken();
                return new NumberType();
            }
            if (currentToken.Type == TokenType.Rw_String)
            {
                currentToken = _lexer.NextToken();
                return new StringType();
            }
            else
            {
                throw new ParserException("Se esperaba tipo");
            }
        }

        private List<StatementNode> Lista_Sentencias()
        {
            //Sentencia Lista_Sentencias
            if (currentToken.Type == TokenType.Rw_Read || currentToken.Type == TokenType.Rw_Print || currentToken.Type == TokenType.Id || currentToken.Type == TokenType.Rw_For)
            {
                var singleStatement = Sentencia();
                var statementList= Lista_Sentencias();
                statementList.Insert(0,singleStatement);
                return statementList;
            }
            //Epsilon
            else
            {
                return new List<StatementNode>();
            }
        }

        private StatementNode Sentencia()
        {
            //read id;
            if (currentToken.Type == TokenType.Rw_Read)
            {
                currentToken = _lexer.NextToken();
                if (currentToken.Type != TokenType.Id)
                    throw new ParserException("Se esperaba Id");
                var variableName = currentToken.Lexeme;
                currentToken = _lexer.NextToken();
                if (currentToken.Type != TokenType.SemiColon)
                    throw new ParserException("Se esperaba ;");
                currentToken = _lexer.NextToken();
                return new ReadNode {Variable = new IdNode{Name = variableName} };
            }
            //print ExpresionBooleana;
            else if (currentToken.Type == TokenType.Rw_Print)
            {
                currentToken = _lexer.NextToken();
                var valueNode =ExpresionBooleana();
                if (currentToken.Type != TokenType.SemiColon)
                    throw new ParserException("Se esperaba ;");
                currentToken = _lexer.NextToken();
                return new PrintNode {Value =valueNode };
            }
            //id = ExpresionBooleana;
            else if (currentToken.Type == TokenType.Id)
            {
                var idNode = ID();
                if (currentToken.Type != TokenType.Op_Assign)
                    throw new ParserException("Se esperaba =");
                currentToken = _lexer.NextToken();
                var valueNode =ExpresionBooleana();
                if (currentToken.Type != TokenType.SemiColon)
                    throw new ParserException("Se esperaba ;");
                currentToken = _lexer.NextToken();
                return new AssignationNode
                {
                     LeftValue = idNode ,
                     RightValue = valueNode
                };
            }
            else if (currentToken.Type == TokenType.Rw_For)
            {
                currentToken = _lexer.NextToken();
                var  idNode=ID();
                if (currentToken.Type != TokenType.Op_Assign)
                    throw new ParserException("Se esperaba = en for");
                currentToken = _lexer.NextToken();
                var initialExpresion = ExpresionBooleana();
                if (currentToken.Type != TokenType.Rw_To)
                    throw new ParserException("Se esperaba TO en for");
                currentToken = _lexer.NextToken();
                var finalExpresion = ExpresionBooleana();
                var codigo = Lista_Sentencias();
                if (currentToken.Type != TokenType.Rw_End)
                    throw new ParserException("Se esperaba end en for");
                currentToken = _lexer.NextToken();

                return new ForNode
                {
                    Id=idNode,
                    InitialValue=initialExpresion,
                    FinalValue = finalExpresion,
                    Code = codigo
                };
            }
            //NO HAY EPSILON
            else
            {
                throw new ParserException("Se esperaba inicio de sentencia");
            }
        }

        private ExpresionNode ExpresionBooleana()
        {
           var expresionNode = Expresion();
           return ExpresionBooleanaP(expresionNode);
        }

        private ExpresionNode ExpresionBooleanaP(ExpresionNode paramNode)
        {
            //== Expresion {ExpresionBooleana'1.param = new Equals(ExpresionBooleana'.param,Expresion.node);} ExpresionBooleanaP
            if (currentToken.Type == TokenType.Op_Equals)
            {
                currentToken = _lexer.NextToken();
                var expresionNode = Expresion();
                var operatorNode = new EqualsOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode =  expresionNode 
                };
                return ExpresionBooleanaP(operatorNode);
            }
            //< Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_LessThan)
            {
                currentToken = _lexer.NextToken();
                var expresionNode = Expresion();
                var operatorNode = new LessThanOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = expresionNode
                };
                return ExpresionBooleanaP(operatorNode);
            }
            //<= Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_LessEqualThan)
            {
                currentToken = _lexer.NextToken();
                var expresionNode = Expresion();
                var operatorNode = new LessEqualsThanOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = expresionNode
                };
                return ExpresionBooleanaP(operatorNode);
            }
            //> Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_GreaterThan)
            {
                currentToken = _lexer.NextToken();
                var expresionNode = Expresion();
                var operatorNode = new GreaterThanOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = expresionNode
                };
                return ExpresionBooleanaP(operatorNode);
            }
            //>= Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_GreaterEqualThan)
            {
                currentToken = _lexer.NextToken();
                var expresionNode = Expresion();
                var operatorNode = new GreaterThanOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = expresionNode
                };
                return ExpresionBooleanaP(operatorNode);
            }
            //!= Expresion ExpresionBooleanaP
            else if (currentToken.Type == TokenType.Op_NotEquals)
            {
                currentToken = _lexer.NextToken();
                var expresionNode = Expresion();
                var operatorNode = new NotEqualsOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = expresionNode
                };
                return ExpresionBooleanaP(operatorNode);
            }
            //epsilon
            else
            {
                return paramNode;
            }
        }

        private ExpresionNode Expresion()
        {
            var factorNode = Factor();
            return ExpresionP(factorNode);
        }

        private ExpresionNode ExpresionP(ExpresionNode paramNode)
        {
            //+ Factor  ExpresionP
            if (currentToken.Type == TokenType.Op_Sum)
            {
                currentToken = _lexer.NextToken();
                var factorNode = Factor();
                var operatorNode = new SumOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = factorNode
                };
                return ExpresionP(operatorNode);
            }
            //- Factor ExpresionP
            else if (currentToken.Type == TokenType.Op_Substract)
            {
                currentToken = _lexer.NextToken();
                var factorNode = Factor();
                var operatorNode = new SubstractOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = factorNode
                };
                return ExpresionP(operatorNode);
            }
            //epsilon
            else
            {
                return paramNode;
            }
        }

        private ExpresionNode Factor()
        {
            var terminoNode= Termino();
            return FactorP(terminoNode);
        }

        private ExpresionNode FactorP(ExpresionNode paramNode)
        {
            //* Termino FactorP
            if (currentToken.Type == TokenType.Op_Multiply)
            {
                currentToken = _lexer.NextToken();
                var terminoNode=Termino();
                var operatorNode = new MultiplyOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = terminoNode
                };
                return FactorP(operatorNode);
            }
            // / Termino FactorP
            else if (currentToken.Type == TokenType.Op_Divide)
            {
                currentToken = _lexer.NextToken();
                var terminoNode = Termino();
                var operatorNode = new DivisionOperatorNode
                {
                    LeftOperandNode = paramNode,
                    RightOperandNode = terminoNode
                };
                return FactorP(operatorNode);
            }
            //epsilon
            else
            {
                return paramNode;
            }
        }

        private ExpresionNode Termino()
        {
            if (currentToken.Type == TokenType.Id)
            {
                return ID();
            }
            else if (currentToken.Type == TokenType.Number)
            {
                float value = float.Parse(currentToken.Lexeme);
                currentToken = _lexer.NextToken();
                return new NumberNode{Value = value};
            }
            else if (currentToken.Type == TokenType.LeftParenthesis)
            {
                currentToken = _lexer.NextToken();
                var expresionNode =ExpresionBooleana();
                if (currentToken.Type != TokenType.RightParenthesis)
                    throw new ParserException("Se esperaba un raito parentesis");
                currentToken = _lexer.NextToken();
                return expresionNode;
            }
            else if (currentToken.Type == TokenType.Op_Not)
            {
                currentToken = _lexer.NextToken();
                var terminoNode=Termino();
                return new NotOperatorNode {OperandNode = terminoNode};
            }
            else
            {
                throw new ParserException("Se esperaba un termino");
            }
        }

        private IdNode ID()
        {
            var name = currentToken.Lexeme;
            currentToken = _lexer.NextToken();
            var accesorList= Lista_Accesor();

            return new IdNode { Name = name ,AccesorList = accesorList};
        }

        private Accesor Lista_Accesor()
        {
            if (currentToken.Type == TokenType.Dot || currentToken.Type == TokenType.LeftParenthesis)
            {
                var accesor=SingleAccesor();
                var accesorList = Lista_Accesor();
                accesor.Next = accesorList;
                return accesor;
            }
            else
            {
                return null;
            }
        }

        private Accesor SingleAccesor()
        {
            if (currentToken.Type == TokenType.LeftParenthesis)
            {
                currentToken = _lexer.NextToken();
                var expresionList =Lista_Expresiones();
                if (currentToken.Type != TokenType.RightParenthesis)
                    throw new ParserException("Se esperaba un raito parentesis");
                currentToken = _lexer.NextToken();
                return new ArrayAccesor{Indexes = expresionList};
            }
            else if (currentToken.Type == TokenType.Dot)
            {
                currentToken = _lexer.NextToken();
                if (currentToken.Type != TokenType.Id)
                    throw new ParserException("Se esperaba un id");
                var id = currentToken.Lexeme;
                currentToken = _lexer.NextToken();
                return new FieldAccesor{Id = id};
            }
            else
            {
                throw  new ParserException("Se esperaba un accesor");
            }
        }

        private List<ExpresionNode> Lista_Expresiones()
        {
            var expresioNode = Expresion();
            var expresionList = Lista_ExpresionesP();
            expresionList.Insert(0,expresioNode);
            return expresionList;
        }

        private List<ExpresionNode> Lista_ExpresionesP()
        {
            if (currentToken.Type == TokenType.Comma)
            {
                currentToken = _lexer.NextToken();
                var expresioNode = Expresion();
                var expresionList = Lista_ExpresionesP();
                expresionList.Insert(0, expresioNode);
                return expresionList;
            }
            else
            {
                return new List<ExpresionNode>();
            }
        }
    }
}