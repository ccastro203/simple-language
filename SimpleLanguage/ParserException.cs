﻿    using System;

namespace SimpleLanguage.Specs
{
    public class ParserException : Exception
    {
        public ParserException(string errorMsg):base(errorMsg)
        {
            
        }
    }
}