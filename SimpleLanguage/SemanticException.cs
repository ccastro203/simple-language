﻿using System;

namespace SimpleLanguage
{
    public class SemanticException : Exception
    {
        public SemanticException(string errorMsg):base(errorMsg)
        {
           
        }
    }
}