﻿using System.Collections.Generic;
using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage
{
    public class SymbolsTable
    {
        public readonly static SymbolsTable Instance=null;
        static SymbolsTable()
        {
            Instance = new SymbolsTable();
        }

        readonly SortedDictionary<string,BaseType> _variablesType = new SortedDictionary<string, BaseType>();
        readonly SortedDictionary<string, ExpresionValue> _variablesValue = new SortedDictionary<string, ExpresionValue>();

        public void DeclareVariable(string id, BaseType type)
        {
            if (_variablesType.ContainsKey(id))
                throw new SemanticException("Ya esta declarado el id "+id);
            _variablesType.Add(id,type);
            _variablesValue.Add(id,type.GetDefaultValue());
        }

        public BaseType GetVariableType(string id)
        {
            if (!_variablesType.ContainsKey(id))
                throw new SemanticException("La variable " + id+ " no existe");
            return _variablesType[id];
        }

        public void SetVariableValue(string id, ExpresionValue value)
        {
            _variablesValue[id] = value;
        }

        public ExpresionValue GetVariableValue(string id)
        {
            return _variablesValue[id];
        }
        

    }
}
