﻿using SimpleLanguage.Types;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
   
    public abstract class Accesor
    {
        public Accesor Next { get; set; }

        public abstract BaseType ValidateSemantics(BaseType sourceType);

    }
}