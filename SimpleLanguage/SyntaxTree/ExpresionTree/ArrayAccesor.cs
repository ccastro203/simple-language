﻿using System.Collections.Generic;
using SimpleLanguage.Types;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    
    public  class ArrayAccesor:Accesor
    {
        public List<ExpresionNode> Indexes { get; set; }

        public override BaseType ValidateSemantics(BaseType sourceType)
        {
            if(!(sourceType is ArrayType))
                 throw new SemanticException("Se necesita arreglo pa indexar");
            var arrayType = (ArrayType) sourceType;
            if(Indexes.Count!=arrayType.Dimensions.Count)
                throw new SemanticException("Error en dimensiones");
            foreach (var expresionNode in Indexes)
            {
                var expresionType = expresionNode.ValidateSemantics();
                if(!(expresionType is NumberType))
                    throw new SemanticException("tiene que ser entero en los indices");
            }
            if (Next != null)
                return Next.ValidateSemantics(arrayType.OfType);
            else
            {
                return arrayType.OfType;
            }
        }
    }
}