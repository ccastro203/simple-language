﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    abstract class  BinaryOperatorNode: ExpresionNode
    {
        public ExpresionNode LeftOperandNode { get; set; }
        public ExpresionNode RightOperandNode { get; set; }    
    }
}
