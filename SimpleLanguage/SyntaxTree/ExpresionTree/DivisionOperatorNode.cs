﻿using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    class DivisionOperatorNode:BinaryOperatorNode
    {
        public override float Evaluate()
        {
            return LeftOperandNode.Evaluate() / RightOperandNode.Evaluate();
        }

        public override BaseType ValidateSemantics()
        {
            var leftType = LeftOperandNode.ValidateSemantics();
            var rightType = RightOperandNode.ValidateSemantics();
            if (leftType is NumberType && rightType is NumberType)
                return leftType;
            throw new SemanticException("No se puede dividir " + leftType + " " + rightType);
        }

        public override ExpresionValue Interpret()
        {
            throw new System.NotImplementedException();
        }
    }
}