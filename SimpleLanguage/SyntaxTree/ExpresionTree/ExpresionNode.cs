﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    public abstract class ExpresionNode
    {
        public abstract float Evaluate();

        public abstract BaseType ValidateSemantics();
        public abstract ExpresionValue Interpret();
    }
}
