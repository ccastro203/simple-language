﻿using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    public class IdNode: ExpresionNode
    {
        public string Name { get; set; }
        public Accesor AccesorList { get; set; }
        public override float Evaluate()
        {
            return 0;
        }

        public override BaseType ValidateSemantics()
        {
            var idType= SymbolsTable.Instance.GetVariableType(Name);
            if (AccesorList != null)
                return AccesorList.ValidateSemantics(idType);
            else
            {
                return idType;
            }
        }

        public override ExpresionValue Interpret()
        {
            return SymbolsTable.Instance.GetVariableValue(Name);
        }
    }
}