﻿using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    class LessThanOperatorNode:BinaryOperatorNode
    {
        public override float Evaluate()
        {
            return LeftOperandNode.Evaluate() < RightOperandNode.Evaluate() ? 1 : 0;
        }
        public override BaseType ValidateSemantics()
        {
            var leftType = LeftOperandNode.ValidateSemantics();
            var rightType = RightOperandNode.ValidateSemantics();
            if (leftType is NumberType && rightType is NumberType)
                return leftType;
            if (leftType is StringType && rightType is StringType)
                return new NumberType();
            throw new SemanticException("No se puede sumar " + leftType + " " + rightType);
        }

        public override ExpresionValue Interpret()
        {
            throw new System.NotImplementedException();
        }
    }
}