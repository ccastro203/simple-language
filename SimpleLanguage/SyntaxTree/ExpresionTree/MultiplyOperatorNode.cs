﻿using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    class MultiplyOperatorNode:BinaryOperatorNode
    {
        public override float Evaluate()
        {
            return LeftOperandNode.Evaluate() * RightOperandNode.Evaluate();
        }

        public override BaseType ValidateSemantics()
        {
            var leftType = LeftOperandNode.ValidateSemantics();
            var rightType = RightOperandNode.ValidateSemantics();
            if (leftType is NumberType && rightType is NumberType)
                return leftType;
            throw new SemanticException("No se puede mult " + leftType + " " + rightType);
        }

        public override ExpresionValue Interpret()
        {
            var leftValue = LeftOperandNode.Interpret();
            var rightValue = RightOperandNode.Interpret();

            if (leftValue is NumberValue && rightValue is NumberValue)
                return new NumberValue
                {
                    Value = ((NumberValue)leftValue).Value
                             * ((NumberValue)rightValue).Value
                };
           
            return null;
        }
    }
}