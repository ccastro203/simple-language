﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    class NumberNode: ExpresionNode
    {
        public float Value { get; set; }
        public override float Evaluate()
        {
            return Value;
        }

        public override BaseType ValidateSemantics()
        {
            return new NumberType();
        }

        public override ExpresionValue Interpret()
        {
            return new NumberValue{Value = Convert.ToDecimal(this.Value)};
        }
    }
}
