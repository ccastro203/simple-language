﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    class SumOperatorNode:BinaryOperatorNode
    {
        public override float Evaluate()
        {
            return LeftOperandNode.Evaluate() + RightOperandNode.Evaluate() ;
        }

        public override BaseType ValidateSemantics()
        {
            var leftType = LeftOperandNode.ValidateSemantics();
            var rightType = RightOperandNode.ValidateSemantics();
            if (leftType is NumberType && rightType is NumberType)
                return leftType;
            if (leftType is StringType && rightType is StringType)
                return leftType;
            throw new SemanticException("No se puede sumar " + leftType + " " + rightType);
        }

        public override ExpresionValue Interpret()
        {
            var leftValue = LeftOperandNode.Interpret();
            var rightValue = RightOperandNode.Interpret();

            if(leftValue is NumberValue && rightValue is NumberValue)
                return new NumberValue
                {
                    Value = ((NumberValue)leftValue).Value 
                             + ((NumberValue)rightValue).Value
                };
            else if(leftValue is StringValue && rightValue is StringValue)
                return new StringValue
                {
                    Value = ((StringValue)leftValue).Value
                             + ((StringValue)rightValue).Value
                };
            return null;
        }
    }
}
