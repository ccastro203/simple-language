﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.ExpresionTree
{
    class UnaryOperator:ExpresionNode
    {
        public ExpresionNode OperandNode { get; set; }
        public override float Evaluate()
        {
            return  OperandNode.Evaluate()*2;
        }

        public override BaseType ValidateSemantics()
        {
            var operandType = OperandNode.ValidateSemantics();
           
           
            if (operandType is NumberType)
                return new NumberType();
            throw new SemanticException("No se puede negar un " + operandType );
        }

        public override ExpresionValue Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
