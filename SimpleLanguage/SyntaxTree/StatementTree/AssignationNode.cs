﻿using SimpleLanguage.SyntaxTree.ExpresionTree;

namespace SimpleLanguage.SyntaxTree.StatementTree
{
    //a = b+4;
    public class AssignationNode:StatementNode
    {
        public IdNode LeftValue { get; set; }
        public ExpresionNode RightValue { get; set; }
        public override void ValidateSemantics()
        {
            var leftType = LeftValue.ValidateSemantics();
            var rightType = RightValue.ValidateSemantics();

            if(!leftType.IsAssignable(rightType))
                throw  new SemanticException("No se puede asignar "+leftType+" "+rightType);

        }

        public override void Interpret()
        {
            var rightValue = RightValue.Interpret();
            SymbolsTable.Instance.SetVariableValue(LeftValue.Name,rightValue);
        }
    }
}