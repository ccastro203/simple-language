using System;
using System.Collections.Generic;
using SimpleLanguage.SyntaxTree.ExpresionTree;
using SimpleLanguage.Types;
using SimpleLanguage.Values;

namespace SimpleLanguage.SyntaxTree.StatementTree
{
    public class ForNode : StatementNode
    {
       

        public IdNode Id { get; set; }
        public ExpresionNode InitialValue { get; set; }
        public ExpresionNode FinalValue { get; set; }
        public List<StatementNode> Code { get; set; }

        public override void ValidateSemantics()
        {
            var idType = Id.ValidateSemantics();
            if(!(idType is NumberType))
                 throw new SemanticException("Tiene que ser numero la var del for");
            var initialType = InitialValue.ValidateSemantics();
            if (!(initialType is NumberType))
                throw new SemanticException("Tiene que ser numero el valor initial del for");
            var finalType = FinalValue.ValidateSemantics();
            if (!(finalType is NumberType))
                throw new SemanticException("Tiene que ser numero la valor final  del for");
            foreach(var statement in Code)
                statement.ValidateSemantics();

        }

        public override void Interpret()
        {
            var initialVal = (NumberValue)InitialValue.Interpret();
            var finalVal = (NumberValue)FinalValue.Interpret();

            SymbolsTable.Instance.SetVariableValue(Id.Name,initialVal);
            while (true)
            {
                var idValue = (NumberValue)SymbolsTable.Instance.GetVariableValue(Id.Name);
                if (idValue.Value > finalVal.Value)
                    break;

                foreach (var statement in Code)
                    statement.Interpret();

                idValue = (NumberValue)SymbolsTable.Instance.GetVariableValue(Id.Name);
                idValue.Value ++;

            }
        }
    }
}