﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleLanguage.SyntaxTree.ExpresionTree;

namespace SimpleLanguage.SyntaxTree.StatementTree
{
    //print A+1;
    public class PrintNode:StatementNode
    {
        public ExpresionNode Value { get; set; }
        public override void ValidateSemantics()
        {
            Value.ValidateSemantics();
        }

        public override void Interpret()
        {
            dynamic value = Value.Interpret();
            Console.WriteLine(value.Value);
        }
    }
}
