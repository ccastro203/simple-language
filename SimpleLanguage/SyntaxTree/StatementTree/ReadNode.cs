﻿using System;
using SimpleLanguage.SyntaxTree.ExpresionTree;
using SimpleLanguage.Types;

namespace SimpleLanguage.SyntaxTree.StatementTree
{
    //read a;
    public class ReadNode:StatementNode
    {
        public IdNode Variable { get; set; }
        public override void ValidateSemantics()
        {
            var variableType = Variable.ValidateSemantics();
            if (!(variableType is NumberType ||
                variableType is StringType))
                throw new SemanticException("No se puede leer "+variableType);
        }

        public override void Interpret()
        {
           var inputValue =  Console.ReadLine();
            var variableType = SymbolsTable.Instance.GetVariableType(Variable.Name);
            var value = variableType.Parse(inputValue);
            SymbolsTable.Instance.SetVariableValue(Variable.Name,value);
        }
    }
}
