﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLanguage.SyntaxTree.StatementTree
{
    public abstract class StatementNode
    {
        public abstract void ValidateSemantics();
        public abstract void Interpret();
    }
}
