﻿using System.Collections.Generic;
using SimpleLanguage.Values;

namespace SimpleLanguage.Types
{
    class ArrayType : BaseType
    {
        public List<int> Dimensions { get; set; }
        public BaseType OfType { get; set; }
        public override bool IsAssignable(BaseType type)
        {
            if (!(type is ArrayType))
                return false;
            var arrayType = (ArrayType) type;
            if (arrayType.Dimensions.Count != Dimensions.Count)
                return false;
            for(int i=0;i<Dimensions.Count;i++)
                if (arrayType.Dimensions[i] != Dimensions[i])
                    return false;
            return OfType.IsAssignable(arrayType.OfType);
        }

        public override ExpresionValue GetDefaultValue()
        {
            int size = 1;
            foreach (var dimension in Dimensions)
                size *= dimension;
            return new ArrayValue{Value = new List<ExpresionValue>(size)};
        }

        public override ExpresionValue Parse(string inputValue)
        {
            throw new System.NotImplementedException();
        }
    }
}