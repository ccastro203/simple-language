﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleLanguage.Values;

namespace SimpleLanguage.Types
{
   
    public abstract class BaseType
    {
         public abstract bool  IsAssignable(BaseType type);
        public abstract ExpresionValue GetDefaultValue();
        public abstract ExpresionValue Parse(string inputValue);
    }
}
