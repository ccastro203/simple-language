﻿using System.Runtime.InteropServices.WindowsRuntime;
using SimpleLanguage.Values;

namespace SimpleLanguage.Types
{
    class NumberType : BaseType
    {
        public override bool IsAssignable(BaseType type)
        {
            return type is NumberType;
        }

        public override ExpresionValue GetDefaultValue()
        {
            return new NumberValue{Value=0};
        }

        public override ExpresionValue Parse(string inputValue)
        {
            return new NumberValue {Value = decimal.Parse(inputValue)};
        }
    }
}