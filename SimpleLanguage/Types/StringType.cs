﻿using SimpleLanguage.Values;

namespace SimpleLanguage.Types
{
    public class StringType : BaseType
    {
        public override bool IsAssignable(BaseType type)
        {
            return type is StringType;
        }

        public override ExpresionValue GetDefaultValue()
        {
            return new StringValue {Value = "Esturingo Value"};
        }

        public override ExpresionValue Parse(string inputValue)
        {
            return new StringValue {Value = inputValue};
        }
    }
}