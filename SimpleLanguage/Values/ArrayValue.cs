﻿using System.Collections.Generic;

namespace SimpleLanguage.Values
{
    public  class ArrayValue:ExpresionValue
    {
        public List<ExpresionValue> Value { get; set; }
    }
}