﻿namespace SimpleLanguage.Values
{
    public  class NumberValue:ExpresionValue
    {
        public decimal Value { get; set; }
    }
}