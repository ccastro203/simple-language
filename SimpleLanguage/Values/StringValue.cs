﻿namespace SimpleLanguage.Values
{
    public  class StringValue:ExpresionValue
    {
        public string Value { get; set; }
    }
}